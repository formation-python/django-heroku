from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('comments/', views.comments_index, name='comments'),
    path('articles/', views.ArticlesList.as_view()),
    path('categories/', views.category_index, name='categories'),
    path('categories/<int:pk>/', views.category_detail, name='categories'),
    path('list/', views.ArticleListView.as_view(), name='articles_list'),
    path('<str:slug>',views.ArticleDetailView.as_view(), name='article-detail'),

]